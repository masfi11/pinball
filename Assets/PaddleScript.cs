﻿using UnityEngine;
using System.Collections;

public class PaddleScript : MonoBehaviour {
    Rigidbody rb;
    private float m_Force = 1500;
    private Vector3 m_ForceDirec;
 
    void Start()
    {
        rb = this.gameObject.GetComponent<Rigidbody>();
        m_ForceDirec= Vector3.forward;
    }
	void Update () {

        if (Input.GetKeyDown(KeyCode.A))
        {
            rb.AddForceAtPosition(m_ForceDirec.normalized * -m_Force, transform.position);
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            rb.AddForceAtPosition(m_ForceDirec.normalized * m_Force, transform.position);
        }
    }
}
