﻿using UnityEngine;
using System.Collections;

public class LifeScript : MonoBehaviour {
    public TextMesh m_LifeText;
    private int m_Lifes = 3;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        m_LifeText.text = "Lifes:" + m_Lifes;
        if(m_Lifes==0)
        {
            GameObject.Find("Score").GetComponent<GameOver>().UpdateScore("Game Over");
        }
    }

    public void UpdateLifes(int m_LifeNumber)
    {
        m_Lifes -= m_LifeNumber;
    }
}
